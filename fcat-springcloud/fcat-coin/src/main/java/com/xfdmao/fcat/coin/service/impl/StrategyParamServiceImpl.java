package com.xfdmao.fcat.coin.service.impl;

import com.xfdmao.fcat.coin.entity.StrategyParam;
import com.xfdmao.fcat.coin.mapper.StrategyParamMapper;
import com.xfdmao.fcat.coin.service.StrategyParamService;
import com.xfdmao.fcat.common.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * Created by fier on 2018/09/20
 */
@Service
public class StrategyParamServiceImpl extends BaseServiceImpl<StrategyParamMapper,StrategyParam> implements StrategyParamService {
}
