package com.xfdmao.fcat.coin.controller;

import com.xfdmao.fcat.coin.entity.StrategyParam;
import com.xfdmao.fcat.coin.entity.Token;
import com.xfdmao.fcat.coin.huobi.util.StrategyUtil;
import com.xfdmao.fcat.coin.service.StrategyParamService;
import com.xfdmao.fcat.coin.service.TokenService;
import org.apache.http.HttpException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

/**
 * Created by fier on 2018/10/22
 */
@RestController
public class TimerController {
    private static Logger logger = Logger.getLogger(TimerController.class);

    @Autowired
    private KlineController klineController;

    @Autowired
    private StrategyParamService strategyParamService;
    @Autowired
    private TokenService tokenService;
    private boolean isStratege1Flag = false;//策略1是否真正执行
    @Scheduled(cron = "0 0/1 * * * ?")
    public void stratege1() throws IOException, HttpException {
        if(isStratege1Flag)return;
        isStratege1Flag=!isStratege1Flag;
        while(true){
            List<StrategyParam> strategyParamList = strategyParamService.selectListAll();
            List<Token> tokens = tokenService.selectListAll();
            try{
                for(Token token:tokens){
                    for(StrategyParam strategyParam:strategyParamList){
                        if(strategyParam.getUsername().equals(token.getUsername())){
                            if("EOS".equals(strategyParam.getSymbol())){
                                StrategyUtil.towMaEOS(strategyParam,token);
                            }else if("BTC".equals(strategyParam.getSymbol())){
                                StrategyUtil.towMaBTC(strategyParam,token);
                            }
                        }
                    }
                }
                break;
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        isStratege1Flag=!isStratege1Flag;
    }
    //@Scheduled(cron = "0 0/14 * * * ?")
    public void getBitinfocharts(){
       klineController.queryHistoryKlineAndSave();
    }

/*    @Autowired
    private BtcHourController btcHourController;

    @Autowired
    private HttpController httpController;
    @Scheduled(cron = "0 0/5 * * * ?")
    public void getBitinfocharts(){
        new Thread(() -> httpController.btcByBitinfocharts()).start();
    }
    //@Scheduled(cron = "0 5 8 * * ?")
    public void getTime1(){
        new Thread(() -> btcHourController.getBtcHour("btcusdt","1week","2")).start();
        new Thread(() -> btcHourController.getBtcHour("btcusdt","1mon","2")).start();
        new Thread(() -> btcHourController.getBtcHour("btcusdt","1year","2")).start();
        new Thread(() -> btcHourController.getBtcHour("btcusdt","1day","2")).start();
        new Thread(() -> btcHourController.getBtcHour("btcusdt","60min","26")).start();
        new Thread(() -> btcHourController.getBtcHour("btcusdt","5min","300")).start();
        new Thread(() -> btcHourController.getBtcHour("btcusdt","4hour","7")).start();
    }


    //@Scheduled(cron = "0 0/5 * * * ?")
    public void getTime2(){
        new Thread(() -> btcHourController.getBtcHour("btcusdt","5min","2")).start();
    }

    //@Scheduled(cron = "0 0/60 * * * ?")
    public void getTime3(){
        new Thread(() -> btcHourController.getBtcHour("btcusdt","60min","2")).start();
        new Thread(() -> btcHourController.getBtcHour("btcusdt","4hour","2")).start();
    }*/
}
