package com.xfdmao.fcat.coin.service;

import com.xfdmao.fcat.coin.entity.StrategyParam;
import com.xfdmao.fcat.common.service.BaseService;

/**
 * Created by fier on 2018/09/20
 */
public interface StrategyParamService extends BaseService<StrategyParam>{
}
